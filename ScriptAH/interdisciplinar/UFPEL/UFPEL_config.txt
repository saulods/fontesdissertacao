### field default for read of property 
[scriptsucupira]

diretorio_de_armazenamento_de_cvs : ./cache/interdisciplinar/UFPEL/cache 
diretorio_de_armazenamento_de_doi : ./cache/interdisciplinar/UFPEL/doi 

email_responsavel_conjunto_cursos : UFPEL@UFPEL.br

modelo_aluno : ./modelos/AL-Alunos.config
modelo_professor: ./modelos/CV_Prof.config
modelo_linha_pesquisa: ./modelos/LP-XXXX.config
modelo_programa_com_alunos: ./modelos/PR-AL-Programa.config
modelo_programas_com_alunos: ./modelos/PR-AL-Programas.config
modelo_programa: ./modelos/PR-Programa.config
modelo_programas: ./modelos/PR-Programas.config
modelo_programa_datas: ./modelos/PR-Programa-Datas.config
modelo_programas_datas: ./modelos/PR-Programas-Datas.config

dataset_professores: ./interdisciplinar/UFPEL/UFPEL_docentes.csv
dataset_alunos: 
separador_csv: |

Universidade_Abreviado : Quadrienio
Universidade_Completo : UNIVERSIDADE FEDERAL DE PERNAMBUCO

############ Exatas #######################
Programa_Abreviado_1 : MD-UFPEL
Programa_Completo_1 : UFPEL
Programa_Alunos_1:

Programa_Abreviado_11 : MM
Programa_Completo_11 : Modelagem Matemática
Programa_Alunos_11: M-MM, D-MM
Programa_arquivo_qualis_de_periodicos_csv_11 : ./qualis/qualis_interdisciplinar_journal_2014.csv
Programa_arquivo_qualis_de_periodicos_pdf_11 : ./qualis/qualis_interdisciplinar_journal_2014.pdf
Programa_area_de_avaliacao_qualis_11: Interdisciplinar