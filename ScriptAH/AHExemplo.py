'''
Created on 7 de out de 2017

@author: mac
'''
import numpy as np
from scipy.cluster.hierarchy import dendrogram, linkage
import matplotlib.pyplot as plt

if __name__ == '__main__':
    mat = np.array([[10.0],
                    [11.0],
                    [80.0],
                    [90.0],
                    [60.0],
                    [55.9],
                    [50.0],
                    [40.0]])

    dist_mat = mat
    linkage_matrix = linkage(dist_mat, "average")
    
    plt.clf()
    
    ddata = dendrogram(linkage_matrix,
                       color_threshold=1,orientation='left',
                       labels=["a", "b", "c", "d",'e', 'f','G' ,'h' ])
    
    # Assignment of colors to labels: 'a' is red, 'b' is green, etc.
    label_colors = {'a': 'r', 'b': 'g', 'c': 'b', 'd': 'm'}
    

    
    plt.show()
    
    
    pass