'''
Created on 8 de out de 2017

@author: mac
'''
import sys
import os
import csv

def lerCSVAH(file,separador):
    d={}
    with open(file,'rb') as csvfile:
        rows = csv.reader(csvfile, delimiter=separador, quoting=csv.QUOTE_NONE)
        for row in rows:
            k= row[1]
            d[k[2:]] =row
    return d

def findDadosCurso(curso,dictAH):
    row = dictAH[curso]
    return row

def lerCSV(f,separador, curso, c, rowDict):
    d={}
    with open(f,'rb') as csvfile:
        rows = csv.reader(csvfile, delimiter=separador, quoting=csv.QUOTE_NONE)
        count= 0
        for row in rows:
            if(count>0):
                c.writerow([rowDict[0],curso,rowDict[3],rowDict[4], rowDict[5], rowDict[6], row[2]])
            count+=1   
    return d

if __name__ == '__main__':
    c = csv.writer(open("./inputdataset.csv", "wb"), delimiter ='|')
    c.writerow(["indice","curso", 'IndProdArt', 'IndProdSup','Orientacao', 'IndProd', 'doscente'])
    f='./interdisciplinar/'
    ah='./csvScriptAh.csv'
    dictAH = lerCSVAH(ah,'|')
    
    for pasta in os.listdir(f):
        if(not pasta.startswith('.')):
            for arq in os.listdir(f+pasta):
                if(arq.endswith('csv')):
                    row=findDadosCurso(pasta,dictAH)
                    lerCSV(f+pasta+'/'+arq, '|', pasta, c, row)
