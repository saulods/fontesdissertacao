#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xml.etree.ElementTree as ET
import csv
import re
import sys
import os, errno, copy, shutil
import datetime 
import string
import operator
from unicodedata import normalize
from collections import namedtuple
from operator import contains
from xml.parsers import expat


class CvLattesProfessorResumo:
 
	nome=None
	idLattes=None
	quantidadeOrientacaoMestradoConcluido = None
	quantidadeOrientacaoDoutoradoConcluido = None
	quantidadeOrientacaoIniciacaoCientificaEmAndamento = None
	quantidadeOrientacaoMestradoEmAndamento = None
	quantidadeOrientacaoDoutoradoEmAndamento = None
	quantidadeApresentacaoTrabalho = None
	quantidadeArtigosEmRevista = None
	quantidadeTrabalhoCompletoCongresso = None
	quantidadeArtigosEmPeriodicos = None
	quantidadeProjetosPesquisa = None
	quantidadeParticipacaoEvento = None
	projetosDePesquisa = None
	
	def __init__(self):
		self.nome = ""
		self.idLattes = 0
		self.quantidadeOrientacaoMestradoConcluido = 0
		self.quantidadeOrientacaoIniciacaoCientificaEmAndamento = 0
		self.quantidadeOrientacaoMestradoEmAndamento = 0
		self.quantidadeOrientacaoDoutoradoEmAndamento = 0
		self.quantidadeApresentacaoTrabalho = 0
		self.quantidadeArtigosEmRevista = 0
		self.quantidadeTrabalhoCompletoCongresso = 0
		self.quantidadeArtigosEmPeriodicos = 0
		self.quantidadeProjetosPesquisa = 0
		self.quantidadeParticipacaoEvento = 0
		self.projetosDePesquisa = 0

		
def parseCSVScriptLattes(listaDeCurriculos, file, separador):
	with open(file,'rb') as csvfile:
		rows = csv.reader(csvfile, delimiter=separador, quoting=csv.QUOTE_NONE)
		for row in rows:
			print(row[0],row[7])
			
			
def parseXMLScriptLattes(xmlTree):	
	listaDeCurriculos=[]

	for curriculoPesquisador in xmlTree.findall('pesquisador'):
		curriculo = CvLattesProfessorResumo()
		curriculo.id   = curriculoPesquisador.get('id')
		for identificacao in curriculoPesquisador.findall('identificacao'):
			curriculo.nome = identificacao.find('nome_completo').text
			
		for participacaoEvento in curriculoPesquisador.findall('participacao_evento'):
			for evento in participacaoEvento.findall('evento'):
				curriculo.quantidadeParticipacaoEvento+=1
				
		for trabalhoApresentado in curriculoPesquisador.findall('apresentacao_trabalho'):
			for trabalho in trabalhoApresentado.findall('trabalho_apresentado'):
				curriculo.quantidadeApresentacaoTrabalho+=1
				
		for orientacaoIniciacaoCientifica in curriculoPesquisador.findall('orientacao_iniciacao_cientifica_concluido'):
			for iniciacaoCientifica in orientacaoIniciacaoCientifica.findall('iniciacao_cientifica'):
				curriculo.quantidadeOrientacaoIniciacaoCientificaEmAndamento+=1	

		for orientacaoMestrado in curriculoPesquisador.findall('orientacao_mestrado_concluido'):
			for orientacaoConcluida in orientacaoMestrado.findall('dissertacao'):
				curriculo.quantidadeOrientacaoMestradoConcluido+=1	
				
		for orientacaoMestrado in curriculoPesquisador.findall('orientacao_mestrado_em_andamento'):
			for orientacaoConcluida in orientacaoMestrado.findall('dissertacao'):
				curriculo.quantidadeOrientacaoMestradoEmAndamento+=1			

		for orientacaoDoutoradoConcluido in curriculoPesquisador.findall('orientacao_doutorado_concluido'):
			for tese in orientacaoDoutoradoConcluido.findall('tese'):
				curriculo.quantidadeOrientacaoDoutoradoConcluido+=1		

		for orientacaoDoutoradoEmAndamento in curriculoPesquisador.findall('orientacao_doutorado_em_andamento'):
			for tese in orientacaoDoutoradoEmAndamento.findall('tese'):
				curriculo.quantidadeOrientacaoDoutoradoEmAndamento+=1			


		for projetosDePesquisa in curriculoPesquisador.findall('projetos_pesquisa'):
			for projeto in projetosDePesquisa.findall('projeto'):
				curriculo.projetosDePesquisa+=1						
		
		
		
		listaDeCurriculos = listaDeCurriculos+[curriculo]
	
	return 	listaDeCurriculos

if __name__ == "__main__":
	
	#print re.sub(r'[^\w.]', '', string)
	
	#re.sub('[^a-zA-Z0-9-_*.]', '', my_string)
	
	data=None
	with open('PR-PPGI-database.xml') as f: 
		print re.sub(r'[^\w.]', '', f.readlines())
		data += re.sub(r'[^\w.]', '', f.readlines())
	
	parser = ET.XMLParser(encoding="utf-8")
	#parser = ET.XMLParser(recover=True)
	xmlTree = ET.parse('PR-PPGI-database.xml', parser=parser)
	
	listaDeCurriculos = parseXMLScriptLattes(xmlTree)
	for curriculo in parseXMLScriptLattes(xmlTree):
		print "pesquisador: ", curriculo.nome,"\n"
		print "id Lattes: ", curriculo.id,"\n"
		print "quantidade de Participacao em Eventos: ", curriculo.quantidadeParticipacaoEvento,"\n"
		print "quantidade de Trabalhos Apresentados: ", curriculo.quantidadeApresentacaoTrabalho,"\n"
		
		print "quantidade de Orientacao de Mestrado em Andamento: ", curriculo.quantidadeOrientacaoMestradoConcluido,"\n"
		print "quantidade de Orientacao de Mestrado Concluido: ", curriculo.quantidadeOrientacaoMestradoConcluido,"\n"
		
		print "quantidade de Orientacao de Doutorado em Andamento: ", curriculo.quantidadeOrientacaoDoutoradoEmAndamento,"\n"
		print "quantidade de Orientacao de Doutorado Concluido: ", curriculo.quantidadeOrientacaoDoutoradoConcluido,"\n"
		
		print "quantidade de Orientacao de Iniciacao Cientifica: ", curriculo.quantidadeOrientacaoIniciacaoCientificaEmAndamento,"\n"
		
		print "quantidade de Projeto de Pesquisa: ", curriculo.quantidadeOrientacaoIniciacaoCientificaEmAndamento,"\n"
	
	parseCSVScriptLattes(listaDeCurriculos, "porMembro.csv", "\t")