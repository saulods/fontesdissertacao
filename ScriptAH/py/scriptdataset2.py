'''
Created on 15 de jun de 2016
'''
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster 
import pandas
from scipy.spatial.distance import pdist
from matplotlib import pyplot as plt

def scriptDist(Ci, Cj): 
    return w_rl * abs(Ci[0]-Cj[0]) + w_curso * abs(Ci[1]-Cj[1]) + w_area * abs(Ci[2]-Cj[2]);

def scriptDistByPublicacao(Ci, Cj): 
    return w_rl * abs(Ci[0]-Cj[0]) + w_curso * abs(Ci[1]-Cj[1]) + w_area * abs(Ci[2]-Cj[2]);

def scriptDistOrientacao(Ci, Cj): 
    return w_rl * abs(Ci[0]-Cj[0]) + w_curso * abs(Ci[1]-Cj[1]) + w_area * abs(Ci[2]-Cj[2]);

def writeCsvByCluster(clusterData, csvFileBaseName, csvFileNameByClusterJoin):
    dataSetBase = pandas.read_csv(csvFileBaseName,sep=";");
    df = pandas.DataFrame(clusterData,columns=['Clusters']) # A is a numpy 2d array
    df=df.join(dataSetBase)
    df.to_csv(csvFileNameByClusterJoin,';',index=False) 
    
def clusterGenerate(tree, k):
    clusters = fcluster(tree, k, criterion='maxclust')
    return clusters

def plotDendrogram(tree, p, descriptionDendrogram):
    plt.figure(figsize=(5, 5))
    plt.title('Hierarchical Clustering Dendrogram \n{}'.format(descriptionDendrogram))
    plt.xlabel('Curriculum number of clusters')
    plt.ylabel('Clusters distance')
    dendrogram(tree,p,show_contracted=False, truncate_mode='lastp', leaf_font_size=8.)
    plt.show()

if __name__ == '__main__':
    
    ##Load Data Set
    #dataSetTable = pandas.read_csv('datasettable.csv',sep=";",usecols=('INFO_AREA','INFO_CURSO','INFO_LINHADEPESQUISA')) # To read 1st,2nd and 4th columns
    #dataSet = dataSetTable.values
    dataSetTable = pandas.read_csv('datasettable.csv',sep=";",usecols=('ID_LATTES')) # To read 1st,2nd and 4th columns
    dataSet = dataSetTable.values
    
    #Set Weight
    w_rl=1
    w_curso=w_rl+dataSet[:,2].max()
    w_area=w_curso+dataSet[:,1].max()
    #Process Hierarchical Clustering 
    tree1 = linkage(dataSet, method='single', metric= lambda u, v: scriptDist(u,v))
    
    #Process Hierarchical Clustering another implemantation
    dataSetTemp = dataSetTable.values
    dataSetDistance =  pdist(dataSetTemp, lambda u, v: scriptDist(u,v))
    tree2 = linkage(dataSetDistance, method='single')
    
    #Plot
    plotDendrogram(tree1, 4,  "by Area");
    plotDendrogram(tree1, 11, "by Course");
    plotDendrogram(tree1, 27, "by Research Line");
    plotDendrogram(tree1, 193,"by all Curriculum");



    #Generate Clusters  
    firstClustersByUniversity     = clusterGenerate(tree1,1);
    secondClusterByArea           = clusterGenerate(tree1,4);
    thirdClusterByCourse          = clusterGenerate(tree1,11);
    fourthClusterByResearchLine   = clusterGenerate(tree1,27);
    fifthClusterByAllCurriculums  = clusterGenerate(tree1,193);
    
    writeCsvByCluster(firstClustersByUniversity,'datasettable.csv', 'firstClustersByUniversity.csv');
    writeCsvByCluster(secondClusterByArea,'datasettable.csv', 'secondClusterByArea.csv');
    writeCsvByCluster(thirdClusterByCourse,'datasettable.csv', 'thirdClusterByCourse.csv');
    writeCsvByCluster(fourthClusterByResearchLine,'datasettable.csv', 'fourthClusterByResearchLine.csv');
    writeCsvByCluster(fifthClusterByAllCurriculums,'datasettable.csv', 'fifthClusterByAllCurriculums.csv');
    
    print firstClustersByUniversity
    pass