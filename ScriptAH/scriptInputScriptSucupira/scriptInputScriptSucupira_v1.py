'''
Created on 14 de set de 2017

@author: mac
'''
import csv

class Docente:
    codigo=None
    idLattes=None
    nome=None
    tipoContato=None
    responsavel=None
    periodoInicio=None
    periodoFim=None
    
    def __init__(self):
        self.codigo=0
        self.idLattes=0
        self.nome = ""
        self.tipoContato = 0
        self.responsavel = 0
        self.periodoInicio = 0
        self.periodoInicio = 0
        
class Universidade:  
    nome=None
    sigla=None
    listaDeDocentes=[]
    
    def __init__(self):
        self.nome=""
        self.sigla=None
        self.listaDeDocentes = []


def printDocentesDeUniversidade(universidade):
    print 'Universidade:  ', universidade.sigla
    for docente in universidade.listaDeDocentes:
        print  '  --> docente: ', docente.nome, ', ', docente.codigo, ', ', docente.idLattes
    print '\n'
          
    
if __name__ == '__main__':
    
    periodoInicio='2013'
    periodoFim = '2016'
    separador = ','
    arquivoCSV = './arquivos_interdisciplinar/ProfessoresECursosInterdisciplinar.csv'
    arquivoCSVIdsLattes = './arquivos_interdisciplinar/idDocentes.csv'

    listaDeUniversidades=[]
    
    dictUniversidade={}
    dictIdLattesDosDocentes={}

    
    with open(arquivoCSV,'rb') as csvfile:
        rows = csv.reader(csvfile, delimiter=separador, quoting=csv.QUOTE_NONE)
        count = 0
        for row in rows:
            #print row[0], row[1], row[7]
            if(count > 0):
                siglaUniversidade = row[1] #pega a sigla da Universidade
                universidade = dictUniversidade.get(siglaUniversidade)
                if(universidade == None):
                    
                    universidadeObject = Universidade()
                    universidadeObject.sigla=row[1]
                    universidadeObject.nome=row[2]
                
                    docenteObject = Docente()
                    docenteObject.nome=row[0]
                    docenteObject.periodoInicio = periodoInicio
                    docenteObject.periodoFim = periodoFim
            
                    universidadeObject.listaDeDocentes+=[docenteObject]
                    dictUniversidade.update({siglaUniversidade:universidadeObject})
                    
                    #printDocentesDeUniversidade(universidadeObject)

                else:        
                    docenteObject = Docente()
                    docenteObject.nome=row[0]
                    docenteObject.periodoInicio = periodoInicio
                    docenteObject.periodoFim = periodoFim
                    universidade.listaDeDocentes+=[docenteObject]
                    #printDocentesDeUniversidade(universidade)

                    
        
            count+=1       
    
    print dictUniversidade
    
    with open(arquivoCSVIdsLattes,'rb') as csvfile:
        rows = csv.reader(csvfile, delimiter='|', quoting=csv.QUOTE_NONE)
        count = 0
        for row in rows:
            docente = Docente()
            docente.codigo = row[0]
            docente.nome = row[1]
            docente.idLattes = row[2]
            dictIdLattesDosDocentes.update({docente.nome:docente})
            print 'Docente: ', docente.codigo, ' nome: ', docente.nome 
    
    for sigla, universidadeObject in dictUniversidade.items():  
        for docente in universidadeObject.listaDeDocentes:
            docenteIdLattes = dictIdLattesDosDocentes.get(docente.nome)
            if(docenteIdLattes == None):
                print 'XXXXXXXXX -> Docente Nao encontrado', docente.nome
            else: 
                docente.codigo = docenteIdLattes.codigo
                docente.idLattes = docenteIdLattes.idLattes
    
    printDocentesDeUniversidade(universidade)

    
    pass