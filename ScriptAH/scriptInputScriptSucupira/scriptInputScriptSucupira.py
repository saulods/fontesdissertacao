'''
Created on 14 de set de 2017

@author: mac
'''
import csv
import os

class Docente:
    codigo=None
    idLattes=None
    nome=None
    tipoContato=None
    responsavel=None
    periodoInicio=None
    periodoFim=None
    
    def __init__(self):
        self.codigo=0
        self.idLattes=''
        self.nome = ''
        self.tipoContato = 0
        self.responsavel = 0
        self.periodoInicio = 0
        self.periodoFim = 0
        
class Universidade:  
    nome=None
    sigla=None
    listaDeDocentes=[]
    
    def __init__(self):
        self.nome=""
        self.sigla=None
        self.listaDeDocentes = []


def printDocentesDeUniversidade(universidade):
    print 'Universidade:  ', universidade.sigla
    for docente in universidade.listaDeDocentes:
        print  '  --> docente: ', docente.nome, ', ', docente.codigo, ', ', docente.idLattes
    print '\n'
          
    
def gerarArquivoInputSucupira(universidade, path):
    c = csv.writer(open(path+'/'+universidade.sigla+'_docentes.csv', "wb"),delimiter ='|')

    c.writerow(['ID_Lattes','MATRICULA','NOME','CH','mail','LP_1','PROGRAMA_1','CATEG_1','Periodo_Inicio_1','Periodo_Fim_1', 'Resp_prog', 'Resp_LP1'])
    print 'Universidade:  ', universidade.sigla
    countResp = 0
    for docente in universidade.listaDeDocentes:
        responsavel = ''
        if(countResp==0):
            responsavel = universidade.sigla
        email = universidade.sigla+'@'+universidade.sigla+'.br'
        c.writerow([docente.idLattes,'99999',docente.nome,'40',email,universidade.sigla,universidade.sigla
                    ,'P',docente.periodoInicio, docente.periodoFim, responsavel, responsavel])
        print  '  --> docente: ', docente.nome, ', ', docente.codigo, ', ', docente.idLattes
        countResp+=1
    print '\n'
    
if __name__ == '__main__':
    
    periodoInicio='2013'
    periodoFim = '2016'
    separador = '|'
    path = './'
    arquivoCSV = './arquivos_interdisciplinar/idsDocentes_ComUniversidades.csv'
    
    listaDeUniversidades=[]
    
    dictUniversidade={}
    dictIdLattesDosDocentes={}

    with open(arquivoCSV,'rb') as csvfile:
        rows = csv.reader(csvfile, delimiter=separador, quoting=csv.QUOTE_NONE)
        for row in rows:
            #print row
            #print row[0], row[1], row[7]
            siglaUniversidade = row[3] #pega a sigla da Universidade
            universidade = dictUniversidade.get(siglaUniversidade)
            if(universidade == None):
                
                universidadeObject = Universidade()
                universidadeObject.sigla=row[3]
                universidadeObject.nome=row[4]
            
                docenteObject = Docente()
                docenteObject.nome=row[1]
                docenteObject.periodoInicio = periodoInicio
                docenteObject.periodoFim = periodoFim
                docenteObject.idLattes = row[2]
                docenteObject.codigo = row[0]

                universidadeObject.listaDeDocentes+=[docenteObject]
                dictUniversidade.update({siglaUniversidade:universidadeObject})
                #printDocentesDeUniversidade(universidadeObject)
            else:        
                docenteObject = Docente()
                docenteObject.nome=row[1]
                docenteObject.periodoInicio = periodoInicio
                docenteObject.periodoFim = periodoFim
                docenteObject.idLattes = row[2]
                docenteObject.codigo = row[0]
                universidade.listaDeDocentes+=[docenteObject]
                #printDocentesDeUniversidade(universidade)
    
    for sigla, universidadeObject in dictUniversidade.items(): 
        os.makedirs(path+'arquivos/'+universidadeObject.sigla)
        gerarArquivoInputSucupira(universidadeObject, path+'arquivos/'+universidadeObject.sigla)
        printDocentesDeUniversidade(universidadeObject)

    
    pass