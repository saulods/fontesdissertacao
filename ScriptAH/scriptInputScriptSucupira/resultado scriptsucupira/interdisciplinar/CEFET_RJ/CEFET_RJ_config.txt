### field default for read of property 
[scriptsucupira]

diretorio_de_armazenamento_de_cvs : ./cache/interdisciplinar/CEFET_RJ/cache 
diretorio_de_armazenamento_de_doi : ./cache/interdisciplinar/CEFET_RJ/doi 

email_responsavel_conjunto_cursos : CEFET_RJ@CEFET_RJ.br

modelo_aluno : ./modelos/AL-Alunos.config
modelo_professor: ./modelos/CV_Prof.config
modelo_linha_pesquisa: ./modelos/LP-XXXX.config
modelo_programa_com_alunos: ./modelos/PR-AL-Programa.config
modelo_programas_com_alunos: ./modelos/PR-AL-Programas.config
modelo_programa: ./modelos/PR-Programa.config
modelo_programas: ./modelos/PR-Programas.config
modelo_programa_datas: ./modelos/PR-Programa-Datas.config
modelo_programas_datas: ./modelos/PR-Programas-Datas.config

dataset_professores: ./interdisciplinar/CEFET_RJ/CEFET_RJ_docentes.csv
dataset_alunos: 
separador_csv: |

Universidade_Abreviado : Quadrienio
Universidade_Completo : CENTRO FEDERAL DE EDUCAÇÃO TECN. CELSO SUCKOW DA FONSECA

############ Exatas #######################
Programa_Abreviado_1 : MD-CEFET_RJ
Programa_Completo_1 : CEFET_RJ
Programa_Alunos_1:

Programa_Abreviado_11 : CEFET_RJ
Programa_Completo_11 : CEFET_RJ
Programa_Alunos_11: M-CEFET_RJ, D-CEFET_RJ
Programa_arquivo_qualis_de_periodicos_csv_11 : ./qualis/qualis_interdisciplinar_journal_2014.csv
Programa_arquivo_qualis_de_periodicos_pdf_11 : ./qualis/qualis_interdisciplinar_journal_2014.pdf
Programa_area_de_avaliacao_qualis_11: Interdisciplinar