package filtrodovizinhomaisproximo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import mmlib4j.gui.WindowImages;
import mmlib4j.images.ColorImage;
import mmlib4j.images.GrayScaleImage;
import mmlib4j.images.impl.ImageFactory;
import mmlib4j.images.impl.RGBImage;
import mmlib4j.utils.ImageBuilder;

public class TesteSaulo {

	public static void main(String[] args) throws IOException {
		
		BufferedImage imagembi = ImageIO.read(new File("d:\\feijao1.jpg"));

		int height = imagembi.getHeight();
		int width  = imagembi.getWidth(); 

		for (int w = 0; w < width; w++) {
			for (int h = 0; h < height; h++) {
				int rgb = imagembi.getRGB(w, h);
				
				// int alpha = (rgb >> 24) & 0xff;
				int r = (int) ((rgb & 0x00FF0000) >>> 16); // Red level
				int g = (int) ((rgb & 0x0000FF00) >>> 8); // Green level
				int b = (int) (rgb & 0x000000FF); // Blue level
				System.out.print("[ RGB "+rgb+" (r: "+r+", g: "+g+", b: "+ b+" )]");
				
				double menordistancia =9999999.99;
				
				for (int wi = -1; wi <= 1; wi++) {
					for (int hj = -1; hj <= 1; hj++) {
						
						if(ehPosicaoValida(width, height,w-wi,h-hj ) ){
							int rgbVizinho = imagembi.getRGB(w-wi, h-hj);
							double menorDistanciaLocal = validaDistanciaEntreVizinhos(rgb, rgbVizinho); 							
							if(menorDistanciaLocal < menordistancia){
								imagembi.setRGB(w, h, rgbVizinho);
							}
							
						}
						

//						int rgb = imagembi.getRGB(w, h);
//						
//						// int alpha = (rgb >> 24) & 0xff;
//						int r = (int) ((rgb & 0x00FF0000) >>> 16); // Red level
//						int g = (int) ((rgb & 0x0000FF00) >>> 8); // Green level
//						int b = (int) (rgb & 0x000000FF); // Blue level
//						System.out.print("[ RGB "+rgb+" (r: "+r+", g: "+g+", b: "+ b+" )]");

					}
				}
				
			}
			System.out.println("");
		}
		
		//scaleImageNegativeImage();
		
	}

	
	public static boolean ehPosicaoValida(int width, int height, int dw, int dh ){
		if((dw >= 0 && dh >= 0) && (dw <=width && dh <=height)){
			return true;
		}		
		return false;		
	}
	
	public static double validaDistanciaEntreVizinhos(int distancia1, int distancia2){
		return Math.sqrt(Math.pow(distancia1-distancia2, 2));
		
	}
	
	
	private static void scaleImageNegativeImage() {
		GrayScaleImage grayImage = ImageBuilder.openGrayImage();
		GrayScaleImage grayImageOut = ImageFactory.createGrayScaleImage(grayImage.getWidth(), grayImage.getHeight());
		
		//filtro Negatico
		for(int pixel=0;pixel < grayImage.getSize(); pixel++){
			int level = 255 - grayImage.getPixel(pixel);
			grayImageOut.setPixel(pixel, level);
			
		}

		WindowImages.show(grayImage, "Entrada");
		WindowImages.show(grayImageOut, "Negativo");
	}
	
	
	

}
