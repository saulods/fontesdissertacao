package filtrodovizinhomaisproximo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import mmlib4j.images.GrayScaleImage;
import mmlib4j.utils.ImageBuilder;

public class Filtro1 {

	public static void main(String[] args) {
		try {
			
			int k=8;
			GrayScaleImage grayImage = ImageBuilder.openGrayImage(new File(
					"imagem\\treinamento_ppm\\one02.jpg"));
			System.out.println();
			
			int width = grayImage.getWidth();
			int height = grayImage.getHeight();
			
			System.out.println(width);
			System.out.println(height);

			// for(int pixel=0;pixel < grayImage.getSize(); pixel++){
			// System.out.println(grayImage.getPixel(pixel));
			// }

			for (int w = 0; w < grayImage.getWidth(); w++) {
				for (int h = 0; h < grayImage.getHeight(); h++) {
					//System.out.print(" " + grayImage.getPixel(w, h));
					int corPixel = grayImage.getPixel(w, h);
					
					System.out.print(" "+corPixel);
					
					double menordistancia=9999999999.99;
					int novaCor=corPixel;
					
					for (int wi = -k; wi <= k; wi++) {
						for (int hj = -k; hj <= k; hj++) {							
							if (ehPosicaoValida(width, height, wi - w, hj-h)) {
							//	System.out.println((w-wi)+"-"+(hj-h)+"");
								int corPixelVizinho = grayImage.getPixel(wi-w, hj-h);
								double menorDistanciaLocal = validaDistanciaEntreVizinhos(
										corPixel, corPixelVizinho);
								
								if(corPixel!=corPixelVizinho){
									System.out.println("diferente "+corPixel+"  x "+ corPixelVizinho);
								}
								
								if (menorDistanciaLocal!=0 && menorDistanciaLocal < menordistancia) {
									menordistancia = menorDistanciaLocal;
									novaCor =corPixelVizinho;
								}
							}
						}
					}
					
					if(novaCor != corPixel){
						grayImage.setPixel(w, h, novaCor);
					}
				}
				System.out.println();
			}

			BufferedImage grayImagemWrite = ImageBuilder
					.convertToImage(grayImage);
			
			//processImagem(grayImagemWrite,k);
			
			ImageIO.write(grayImagemWrite, "JPG", new File("feijao2_bw.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void processImagem(BufferedImage grayImagem, int k) {
		int height = grayImagem.getHeight();
		int width = grayImagem.getWidth();

		for (int w = 0; w < width; w++) {
			for (int h = 0; h < height; h++) {
				int rgb = grayImagem.getRGB(w, h);
				printarCor(rgb);
				
				double menordistancia = 9999999.99;
				for (int wi = -k; wi <= k; wi++) {
					for (int hj = -k; hj <= k; hj++) {
						
						if (ehPosicaoValida(width, height, w - wi, h - hj)) {
							int rgbVizinho = grayImagem.getRGB(w - wi, h - hj);
							double menorDistanciaLocal = validaDistanciaEntreVizinhos(
									rgb, rgbVizinho);
							if (menorDistanciaLocal < menordistancia) {
								grayImagem.setRGB(w, h, rgbVizinho);
							}
						}
					}
				}

			}
			System.out.println("");
		}
	}

	
	public static void printarCor(int rgb){

		// int alpha = (rgb >> 24) & 0xff;
		int r = (int) ((rgb & 0x00FF0000) >>> 16); // Red level
		int g = (int) ((rgb & 0x0000FF00) >>> 8); // Green level
		int b = (int) (rgb & 0x000000FF); // Blue level
		System.out.print("[ RGB " + rgb + " (r: " + r + ", g: " + g
				+ ", b: " + b + " )]");
		
	}
	
	
	public static boolean ehPosicaoValida(int width, int height, int dw, int dh ){
		if((dw >= 0 && dh >= 0) && (dw <=width && dh <=height)){
			//System.err.println("dw: "+dw+" dh: "+dh);
			return true;
		}		
		return false;		
	}
	
	public static double validaDistanciaEntreVizinhos(int distancia1, int distancia2){
		return Math.sqrt(Math.pow(distancia1-distancia2, 2));
		
	}
}
