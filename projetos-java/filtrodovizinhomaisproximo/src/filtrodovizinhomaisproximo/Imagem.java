package filtrodovizinhomaisproximo;

public class Imagem {

	
	
	private int pixels[] = null;

	
	public Imagem(int[] pixels){
		this.pixels=pixels;	
	}
	
	public int[] getPixels() {
		return pixels;
	}

	public void setPixels(int[] pixels) {
		this.pixels = pixels;
	} 
	
	public int sizePixels(){
		if(this.pixels!=null){
			return this.pixels.length;
		}else{
			return 0;
		}
	}
}
