package filtrodovizinhomaisproximo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import com.sun.medialib.mlib.Image;

import mmlib4j.gui.WindowImages;
import mmlib4j.images.ColorImage;
import mmlib4j.images.GrayScaleImage;
import mmlib4j.images.impl.ImageFactory;
import mmlib4j.utils.ImageBuilder;

public class Segmentacao {

	public ColorImage[]  lerPixelsParaTreinamento(final File diretorioImagensTreinamento){
		ColorImage[] pixelsImagem = new ColorImage[diretorioImagensTreinamento.listFiles().length];
		int i=0;
		for(File fileImage: diretorioImagensTreinamento.listFiles()){
			ColorImage image = ImageBuilder.openRGBImage(fileImage);
			pixelsImagem[i]=image;
			i++;
		}
		return pixelsImagem;
	}
	
	public double validaDistanciaEntreVizinhos(int distancia1, int distancia2){
		return Math.sqrt(Math.pow(distancia1-distancia2, 2));
		
	}

	public int calculaDistancia(int pixel,  List<Imagem> treinamentoFundo, List<Imagem> trainamentoFeijao ){
		int pi=0;
		
		int pixelMaisProximoFundo=0;
		double menorDistanciaFundo=0;
		
		int pixelMaisProximoFeijao=0;
		double menorDistanciaFeijao=0;		
		
		for (Imagem feijao : trainamentoFeijao) {
			for (int i = 0; i < feijao.sizePixels(); i++) {
				int valorPixelVizinho = feijao.getPixels()[i];
				double menorDistancia = validaDistanciaEntreVizinhos(pixel,valorPixelVizinho);
				if(pixelMaisProximoFeijao==0){
					menorDistanciaFeijao = menorDistancia; 
					pixelMaisProximoFeijao = valorPixelVizinho;
				}else{
					if(menorDistancia < menorDistanciaFeijao){
						menorDistanciaFeijao = menorDistancia; 
						pixelMaisProximoFeijao = valorPixelVizinho;
						
					}
				}
			}			
		}
		
		for (Imagem fundo : treinamentoFundo) {
			for (int i = 0; i < fundo.sizePixels(); i++) {
				int valorPixelVizinho = fundo.getPixels()[i];
				double menorDistancia = validaDistanciaEntreVizinhos(pixel,valorPixelVizinho);
				if(pixelMaisProximoFundo==0){
					menorDistanciaFundo = menorDistancia; 
					pixelMaisProximoFundo = valorPixelVizinho;
				}else{
					if(menorDistancia < menorDistanciaFundo){
						menorDistanciaFundo = menorDistancia; 
						pixelMaisProximoFundo = valorPixelVizinho;
						
					}
				}
			}			
		}
		
		if(pixelMaisProximoFeijao < pixelMaisProximoFundo)
			return pixel-255;
		else
			return pixelMaisProximoFeijao / (pixelMaisProximoFundo + pixelMaisProximoFeijao)*255;
			

	}
	
	public ColorImage processaIMGComAmostras(File amostra, List<Imagem> treinamentoFundo, List<Imagem> treinamentoFeijao){
		ColorImage image = ImageBuilder.openRGBImage(amostra);

		ColorImage novaImagem = ImageFactory.createColorImage(image.getWidth(), image.getHeight());		
		//filtro Negatico
		for(int pixel=0;pixel < image.getSize(); pixel++){		
			int pi = image.getPixel(pixel);
			System.out.println("Pixel: "+pi);
			int level = calculaDistancia(pi , treinamentoFundo, treinamentoFeijao);
			novaImagem.setPixel(pixel, level);			
		}
		WindowImages.show(novaImagem, "Entrada");
		
		return novaImagem;
		
	}
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Segmentacao filtro = new Segmentacao();
		
	    float menor_dist_b=0.0f;
	    float menor_dist_f=0.0f;
		
		float vetorB[][][] = new float [256][256][256];
		
		float vetorF[][][] = new float [256][256][256];

		boolean load=true;
		
		//ColorF
		ColorImage[] treinamentoFundo = filtro.lerPixelsParaTreinamento(new File("imagens/imagens/treinamento/test/fundo/"));
		
		//ColorB
		ColorImage[] treinamentoFeijao = filtro.lerPixelsParaTreinamento(new File("imagens/imagens/treinamento/test/feijao/"));
		System.out.println(new Date());
		
		if(!load){
		
		    for (int r=0; r<=255; r++){
			    for (int g=0; g<=255; g++){
					for (int b=0; b<=255; b++){
						menor_dist_b=999999.0f;
						menor_dist_f=999999.0f;
						for(int i=0; i < treinamentoFeijao.length; i++){
							if(i<treinamentoFundo.length){
								for(int n=0; n<treinamentoFundo[i].getSize(); n++){
									float dist =(float) Math.sqrt(
												Math.pow(r-treinamentoFundo[i].getRed(n),2) + 
												Math.pow(g-treinamentoFundo[i].getGreen(n),2) + 
												Math.pow(b-treinamentoFundo[i].getBlue(n),2)
														);
									if(dist < menor_dist_b) 
										menor_dist_f = dist;
								}
							}
							for(int n=0; n<treinamentoFeijao[i].getSize(); n++){
								float dist =(float) Math.sqrt(
											Math.pow(r-treinamentoFeijao[i].getRed(n),2) + 
											Math.pow(g-treinamentoFeijao[i].getGreen(n),2) + 
											Math.pow(b-treinamentoFeijao[i].getBlue(n),2)
													);
								if(dist < menor_dist_b) 
									menor_dist_b = dist;
							}
						}
					    vetorB[r][g][b] = menor_dist_b;
					    vetorF[r][g][b] = menor_dist_f;
					}
			    }
			    System.out.println(r);
		    }
			
		    
		    ObjectOutputStream out;
			try {
				out = new ObjectOutputStream(new FileOutputStream("vectorb_c.ser"));
		        out.writeObject(vetorB);
		        out.flush();
		        out.close();
		        
				out = new ObjectOutputStream(new FileOutputStream("vectorf_c.ser"));
		        out.writeObject(vetorF);
		        out.flush();
		        out.close();
		        
	//	       
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}else{
			try{

				ObjectInputStream in=null;
				
		        in = new ObjectInputStream(new FileInputStream("vectorb_c.ser"));
		        vetorB = (float[][][]) in.readObject();
		        in.close();
		        
		        
		        in = new ObjectInputStream(new FileInputStream("vectorf_c.ser"));
		        vetorF = (float[][][]) in.readObject();
		        in.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	    
		System.out.println(new Date());

		File amostra = new File("imagens/imagens/amostra/IMG_DS1_001_100_000_000_003.png");
		
		ColorImage imageDeAmostra = ImageBuilder.openRGBImage(amostra);
		ColorImage imagemDeSaida = ImageBuilder.openRGBImage(amostra);
		
		for (int n=0; n<imageDeAmostra.getSize(); n++){
			imagemDeSaida.setPixel(n, (int)(
					vetorF [imageDeAmostra.getRed(n)]
						   [imageDeAmostra.getGray(n)]
						   [imageDeAmostra.getBlue(n)] / 
				    ( 
				    	vetorF[imageDeAmostra.getRed(n)]
				    		  [imageDeAmostra.getGreen(n)]
				    		  [imageDeAmostra.getBlue(n)] + 
				        vetorB[imageDeAmostra.getRed(n)]
				        	  [imageDeAmostra.getGreen(n)]
				        	  [imageDeAmostra.getBlue(n)] 
				     ) )* 255);
		}
		
		BufferedImage bfImage = ImageBuilder.convertToImage(imagemDeSaida);
		try {
			ImageIO.write(bfImage, "JPG", new File("feijao7_bw.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
}
