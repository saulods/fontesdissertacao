package filtrodovizinhomaisproximo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import mmlib4j.gui.WindowImages;
import mmlib4j.images.ColorImage;
import mmlib4j.images.impl.ImageFactory;
import mmlib4j.utils.ImageBuilder;

public class Filtro {

	public List<Imagem>  lerPixelsParaTreinamento(final File diretorioImagensTreinamento){
		List<Imagem> pixelsImagem = new ArrayList<Imagem>();
		for(File fileImage: diretorioImagensTreinamento.listFiles()){
			ColorImage image = ImageBuilder.openRGBImage(fileImage);
			pixelsImagem.add(new Imagem(image.getPixels()));
		}
		return pixelsImagem;
	}
	
	public double validaDistanciaEntreVizinhos(int distancia1, int distancia2){
		return Math.sqrt(Math.pow(distancia1-distancia2, 2));
		
	}
	
	public int calculaDistancia(int pixel,  List<Imagem> treinamentoFundo, List<Imagem> trainamentoFeijao ){
		int pi=0;
		
		int pixelMaisProximoFundo=0;
		double menorDistanciaFundo=0;
		
		int pixelMaisProximoFeijao=0;
		double menorDistanciaFeijao=0;		
		
		for (Imagem feijao : trainamentoFeijao) {
			for (int i = 0; i < feijao.sizePixels(); i++) {
				int valorPixelVizinho = feijao.getPixels()[i];
				double menorDistancia = validaDistanciaEntreVizinhos(pixel,valorPixelVizinho);
				if(pixelMaisProximoFeijao==0){
					menorDistanciaFeijao = menorDistancia; 
					pixelMaisProximoFeijao = valorPixelVizinho;
				}else{
					if(menorDistancia < menorDistanciaFeijao){
						menorDistanciaFeijao = menorDistancia; 
						pixelMaisProximoFeijao = valorPixelVizinho;
						
					}
				}
			}			
		}
		
		for (Imagem fundo : treinamentoFundo) {
			for (int i = 0; i < fundo.sizePixels(); i++) {
				int valorPixelVizinho = fundo.getPixels()[i];
				double menorDistancia = validaDistanciaEntreVizinhos(pixel,valorPixelVizinho);
				if(pixelMaisProximoFundo==0){
					menorDistanciaFundo = menorDistancia; 
					pixelMaisProximoFundo = valorPixelVizinho;
				}else{
					if(menorDistancia < menorDistanciaFundo){
						menorDistanciaFundo = menorDistancia; 
						pixelMaisProximoFundo = valorPixelVizinho;
						
					}
				}
			}			
		}
		
		if(pixelMaisProximoFeijao < pixelMaisProximoFundo)
			return pixel-255;
		else
			return pixelMaisProximoFeijao / (pixelMaisProximoFundo + pixelMaisProximoFeijao)*255;
			

	}
	
	public ColorImage processaIMGComAmostras(File amostra, List<Imagem> treinamentoFundo, List<Imagem> treinamentoFeijao){
		ColorImage image = ImageBuilder.openRGBImage(amostra);

		ColorImage novaImagem = ImageFactory.createColorImage(image.getWidth(), image.getHeight());		
		//filtro Negatico
		for(int pixel=0;pixel < image.getSize(); pixel++){		
			int pi = image.getPixel(pixel);
			System.out.println("Pixel: "+pi);
			int level = calculaDistancia(pi , treinamentoFundo, treinamentoFeijao);
			novaImagem.setPixel(pixel, level);			
		}
		WindowImages.show(novaImagem, "Entrada");
		
		return novaImagem;
		
	}
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Filtro filtro = new Filtro();
		
		//ColorF
		List<Imagem> treinamentoFundo = filtro.lerPixelsParaTreinamento(new File("imagens/imagens/treinamento/fundo/"));
		
		//ColorB
		List<Imagem> treinamentoFeijao = filtro.lerPixelsParaTreinamento(new File("imagens/imagens/treinamento/feijao/"));
		
		File amostra = new File("imagens/imagens/amostra/IMG_DS1_001_100_000_000_003.png");
		
		ColorImage colorImag =  filtro.processaIMGComAmostras(amostra, treinamentoFundo, treinamentoFeijao);
			
		BufferedImage bfImage = ImageBuilder.convertToImage(colorImag);

//processImagem(grayImagemWrite,k);

		try {
			ImageIO.write(bfImage, "JPG", new File("feijao3_bw.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
}
