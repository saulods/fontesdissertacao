package filtrodovizinhomaisproximo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import mmlib4j.images.ColorImage;
import mmlib4j.utils.ImageBuilder;

public class TesteSaulo2 {

	public static void main(String[] args) {
		File amostra = new File("imagens/imagens/amostra/IMG_DS1_001_100_000_000_003.png");
		ColorImage image = ImageBuilder.openRGBImage(amostra);
		
//		for (int width = 0; width < image.getWidth(); width++) {
//			for (int height = 0; height < image.getHeight(); height++) {
//				System.out.print(" "+ image.getBlue(width,height)+"|"+
//										image.getRed(width,height)+"|"+
//										image.getGreen(width,height));
//			}
//			System.out.println();
//		}
		
//		for (int i = 0; i < image.getSize(); i++) {
//			System.out.println(""+image.getBlue(i)+"|"+image.getRed(i)+"|"+image.getBlue(i));
//		}
		
		float vetorB[][][] = new float [256][256][256];
	    ObjectOutputStream out;
		try {
			out = new ObjectOutputStream(new FileOutputStream("test.ser"));
	        out.writeObject(vetorB);
	        out.flush();
	        out.close();
	        
	        
	        ObjectInputStream in = new ObjectInputStream(new FileInputStream("test.ser"));
	        float[][][] array = (float[][][]) in.readObject();
	        in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
